import React from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

/*import HomeScreen from "./src/screens/home/home.screen";*/

const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{
            title: "MINHAS LISTAS",
            headerStyle: {
              backgroundColor: "#413B89",
            },
            headerTintColor: "#FFF",
            headerTitleAlign: "center",
          }}
        />
        <Stack.Screen
          name="Book"
          component={BookScreen}
          options={{
            title: "",
            headerStyle: { backgroundColor: "#413B89" },
            headerTintColor: "#FFF",
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

const HomeScreen = ({ navigation }) => {
  return (
    <View style={{ flex: 1 }}>
      <View style={styles.content}>
        <Text style={styles.contentText}>Popular Now</Text>

        <View style={styles.list}>
          <View style={styles.imageContainer}>
            <TouchableOpacity
              onPress={() =>
                navigation.navigate("Book", {
                  name: "Creative Hustle",
                  author: "Ramen Albert",
                  description:
                    "Dean on Branding presents in a compact form the twenty essential principles of branding that will lead to the creation of strong brands....",
                  price: 35.9,
                })
              }
            >
              <Image
                source={require("./assets/Creative_Hustle_Book.png")}
                style={styles.image}
              />
            </TouchableOpacity>
            <View style={styles.footer}>
              <Text style={styles.footerName}>Creative Hustle</Text>
              <Text style={styles.footerAuthor}>Ramen Albert</Text>
            </View>
          </View>

          <View style={styles.imageContainer}>
            <TouchableOpacity
              onPress={() =>
                navigation.navigate("Book", {
                  name: "Art Unleashed",
                  author: "Stefano Milik",
                  description:
                    "Dean on Branding presents in a compact form the twenty essential principles of branding that will lead to the creation of strong brands....",
                  price: 29.67,
                })
              }
            >
              <Image
                source={require("./assets/Art_Unleashed_Book.png")}
                style={styles.image}
              />
            </TouchableOpacity>
            <View style={styles.footer}>
              <Text style={styles.footerName}>Art Unleashed</Text>
              <Text style={styles.footerAuthor}>Stefano Milik</Text>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

const BookScreen = ({ navigation, route }) => {
  const imageSource =
    route.params.name === "Creative Hustle"
      ? require("./assets/Creative_Hustle_Book.png")
      : require("./assets/Art_Unleashed_Book.png");

  return (
    <View style={{ flex: 1 }}>
      <View style={styles.bookContent}>
        <Image source={imageSource} style={styles.bookImage} />
        <Text style={styles.bookName}>{route.params.name}</Text>
        <Text style={styles.bookAuthor}>{route.params.author}</Text>
      </View>
      <View style={styles.descriptionContent}>
        <Text style={styles.descriptionHeader}>Description</Text>
        <View style={styles.textContent}>
          <Text style={styles.descriptionText}>{route.params.description}</Text>
        </View>
      </View>
      <View style={styles.buttonContainer}>
        <TouchableOpacity>
          <View style={styles.button}>
            <Text style={styles.buttonText}>
              Buy Now For ${route.params.price}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  content: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "white",
    justifyContent: "flex-start",
    alignItems: "center",
    paddingTop: 30,
  },
  contentText: {
    fontSize: 30,
    color: "black",
    paddingBottom: 10,
  },
  bookContent: {
    flex: 3,
    backgroundColor: "#F3E7DD",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  bookImage: {
    width: 175,
    height: 245,
    margin: 8,
    borderWidth: 2,
    borderColor: "black",
    borderRadius: 20,
  },
  bookName: {
    fontSize: 24,
    color: "black",
    paddingTop: 10,
  },
  bookAuthor: {
    fontSize: 18,
    color: "#C1C0C3",
  },
  descriptionContent: {
    flex: 2,
    backgroundColor: "white",
    alignItems: "flex-start",
    justifyContent: "flex-start",
    flexDirection: "column",
    paddingTop: 20,
    paddingLeft: 56,
  },
  descriptionHeader: {
    fontSize: 18,
    color: "black",
  },
  textContent: {
    justifyContent: "center",
    alignItems: "center",
    width: 286,
  },
  descriptionText: {
    fontSize: 16,
    color: "#C1C0C3",
  },
  buttonContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
  },
  button: {
    width: 327,
    height: 56,
    backgroundColor: "#413B89",
    borderRadius: 8,
    justifyContent: "center",
    alignItems: "center",
  },
  buttonText: {
    fontSize: 18,
    color: "white",
  },
  image: {
    width: 140,
    height: 190,
    resizeMode: "contain",
    margin: 8,
  },
  imageContainer: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  backButton: {
    width: 6,
    height: 12,
  },
  footer: {
    flex: 2,
    flexDirection: "column",
    alignItems: "center",
  },
  footerName: {
    fontSize: 16,
    color: "black",
  },
  footerAuthor: {
    fontSize: 12,
    color: "#C1C0C3",
  },
  list: {
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
  },
});

export default App;
