import React from "react";
import { View, Text, TouchableOpacity, Image, StyleSheet } from "react-native";

const HomeScreen = ({ navigation }) => {
  return (
    <View style={{ flex: 1 }}>
      <View style={styles.content}>
        <Text style={styles.contentText}>Popular Now</Text>

        <View style={styles.list}>
          <View style={styles.imageContainer}>
            <TouchableOpacity
              onPress={() =>
                navigation.navigate("Book", {
                  name: "Creative Hustle",
                  author: "Ramen Albert",
                  description:
                    "Dean on Branding presents in a compact form the twenty essential principles of branding that will lead to the creation of strong brands....",
                  price: 35.9,
                })
              }
            >
              <Image
                source={require("./assets/Creative_Hustle_Book.png")}
                style={styles.image}
              />
            </TouchableOpacity>
            <View style={styles.footer}>
              <Text style={styles.footerName}>Creative Hustle</Text>
              <Text style={styles.footerAuthor}>Ramen Albert</Text>
            </View>
          </View>

          <View style={styles.imageContainer}>
            <TouchableOpacity
              onPress={() =>
                navigation.navigate("Book", {
                  name: "Art Unleashed",
                  author: "Stefano Milik",
                  description:
                    "Dean on Branding presents in a compact form the twenty essential principles of branding that will lead to the creation of strong brands....",
                  price: 29.67,
                })
              }
            >
              <Image
                source={require("./assets/Art_Unleashed_Book.png")}
                style={styles.image}
              />
            </TouchableOpacity>
            <View style={styles.footer}>
              <Text style={styles.footerName}>Art Unleashed</Text>
              <Text style={styles.footerAuthor}>Stefano Milik</Text>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  content: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "white",
    justifyContent: "flex-start",
    alignItems: "center",
    paddingTop: 30,
  },
  contentText: {
    fontSize: 30,
    color: "black",
    paddingBottom: 10,
  },
  image: {
    width: 140,
    height: 190,
    resizeMode: "contain",
    margin: 8,
  },
  imageContainer: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  footer: {
    flex: 2,
    flexDirection: "column",
    alignItems: "center",
  },
  footerName: {
    fontSize: 16,
    color: "black",
  },
  footerAuthor: {
    fontSize: 12,
    color: "#C1C0C3",
  },
  list: {
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
  },
});

export default HomeScreen;
